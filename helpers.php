<?php

use Illuminate\Support\Str;

if (!function_exists('entry')) {
    function entry($entry = null, $markup = false, $manifest = 'assets/entries.json')
    {
        static $all;

        $results = [];
        $manifest_path = join_path(ROOT_DIR, $manifest);

        if (!is_file($manifest_path)) {
            return $markup ? '' : [];
        }

        $all = $all ?: json_decode(file_get_contents($manifest_path), true);

        if (!$entry) {
            return $all;
        }

        if (!isset($all[$entry])) {
            return [];
        }

        foreach ($all[$entry] as $i => $value) {
            $ext = pathinfo($value, PATHINFO_EXTENSION);

            switch ($ext) {
                case 'js' :
                    $result = $markup ? sprintf(
                        '<script src="%s" %s async defer></script>',
                        $value,
                        is_array($markup) ? attr($markup, ['media']) : ''
                    ) : $value;
                    break;

                case 'css' :
                    $result = $markup ? sprintf(
                        '<link href="%s" rel="stylesheet" %s>',
                        $value,
                        is_array($markup)
                            ? attr(array_merge($markup, isset($markup['onload']) ? [] : [
                                'media' => 'bogus',
                                'onload' => sprintf('this.media = \'%s\';', isset($markup['media']) ? $markup['media'] : 'all')
                            ]))
                            : 'media="bogus" onload="this.media = \'all\';"'
                    ) : $value;
                    break;
                default :
                    $result = '';
                    break;
            }

            $id = sanitize_title(sprintf('%s-%s', $value, $i));

            $results[$id] = $result;
        }

        return $markup ? implode(PHP_EOL, $results) : $results;
    }
}

if (!function_exists('asset')) {
    function asset($entry = null, $markup = false, $manifest = 'assets/assets.json')
    {
        static $all;

        $manifest_path = join_path(ROOT_DIR, $manifest);

        if (!is_file($manifest_path)) {
            return $markup ? '' : [];
        }

        $all = $all ?: json_decode(file_get_contents($manifest_path), true);

        if (!$entry) {
            return $all;
        }

        if (isset($all[$entry])) {
            return $all[$entry];
        }

        return null;
    }
}

if (!function_exists('preload')) {
    function preload($as, $resource, $expiration = null)
    {
        $pushed = [];
        $expiration = $expiration ?: 60 * 60 * 24 * 30;
        $prev = isset($_COOKIE['pushed'])
            ? json_decode($_COOKIE['pushed'])
            : [];

        if (!is_array($resource)) {
            $resource = [$resource];
        }

        foreach ($resource as $r) {
            $ext = pathinfo($r, PATHINFO_EXTENSION);
            $pushed[] = $r;
            if (!in_array($r, $prev ?: [])) {
                header(sprintf('Link: <%s>; as=%s; rel=preload;', $r, $as), false);
            }
        }

        setcookie('pushed', json_encode($pushed), 0, ($expiration + time()));

        return $resource;
    }
}

if (!function_exists('using_elementor')) {
    function using_elementor($post_id = null)
    {
        return $post_id
            ? defined('ELEMENTOR_VERSION') && Elementor\Plugin::$instance->db->is_built_with_elementor($post_id)
            : defined('ELEMENTOR_VERSION');
    }
}

if (!function_exists('thumbnail')) {
    function thumbnail($id, $size = 'full', $query = [], $strict = true)
    {
        $sizes = Config::get('images')->all();

        if (is_bool($query)) {
            $strict = $query;
            $query = [];
        }

        $src = wp_get_attachment_image_src($id, $size) ?: [];
        $thumbnail = array_shift($src);

        if (!env('IMGIX_SOURCE') && $strict && isset($sizes[$size])) {
            list($width, $height) = $sizes[$size];

            if ($width && $height) {
                if (!is_int(strpos($thumbnail, implode('x', [$width, $height])))) {
                    return false;
                }
            }

            if ($width) {
                if (!is_int(strpos($thumbnail, "-{$width}x"))) {
                    return false;
                }
            }

            if ($height) {
                if (!is_int(strpos($thumbnail, "x{$height}."))) {
                    return false;
                }
            }
        }

        if (env('IMGIX_SOURCE') && $thumbnail) {
            $parts = explode('?', $thumbnail);
            parse_str($parts[1], $original_query);

            if (isset($query['x'])) {
                if (isset($original_query['w'])) {
                    $original_query['w'] *= (float) $query['x'];
                }

                if (isset($original_query['h'])) {
                    $original_query['h'] *= (float) $query['x'];
                }

                unset($query['x']);
            }

            return implode('?', [$parts[0], http_build_query(array_filter(array_merge($original_query, $query)))]);
        }

        return $thumbnail;
    }
}

if (!function_exists('is_blog')) {
    function is_blog()
    {
        return is_home() || is_search() || is_category() || is_tag() || is_author() || is_date() || (is_archive() && get_post_type() === 'post');
    }
}

if (!function_exists('is_divi')) {
    function is_divi()
    {
        return function_exists('et_core_is_builder_used_on_current_request') && et_core_is_builder_used_on_current_request();
    }
}

if (!function_exists('str_number')) {
    function str_number($num = null, $zero = '')
    {
        $num = (int) preg_replace_callback('/[\D]/', function () {
            return '';
        }, explode('.', (string) $num)[0]);

        $words = [];

        $list1 = [
            '', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        ];

        $list2 = [
            '', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred'
        ];

        $list3 = [
            '', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion', 'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        ];

        $levels = (int) ((strlen($num) + 2) / 3);
        $num = substr('00' . $num, $levels * -3);
        $num_levels = str_split($num, 3);

        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int) ($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ($hundreds == 1 ? '' : 's') . ' ' : '');
            $tens = (int) ($num_levels[$i] % 100);
            $singles = '';

            if ($tens < 20) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
            } else {
                $tens = (int) ($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }

            $words[] = $hundreds . $tens . $singles . (($levels && (int) ($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
        }

        $commas = count($words);

        if ($commas > 1) {
            $commas = $commas - 1;
        }

        $word = trim(implode(' ', $words));

        return $word ? $word : $zero;
    }
}

if (!function_exists('show')) {
    function show(...$pieces)
    {
        $glue = '';

        if (isset($pieces[0]) && is_array($pieces[0])) {
            if (isset($pieces[1])) {
                $glue = $pieces[1];
            }

            $pieces = $pieces[0];
        }
        echo implode($glue, $pieces);
    }
}

if (!function_exists('jsh')) {
    function jsh($tag, $attrs = [], $after = null, $on_error = null, $fallback = null)
    {
        return sprintf(
            'h(%s, %s%s);',
            json_encode($tag),
            json_encode($attrs, JSON_UNESCAPED_SLASHES),
            implode('', [
                $after ? sprintf(', function() { %s }', is_callable($after) ? $after() : $after) : ', null',
                $on_error ? sprintf(', function() { %s }', is_callable($on_error) ? $on_error() : $on_error) : ', null',
                $fallback ? sprintf(', "%s"', is_callable($fallback) ? $fallback() : $fallback) : ''
            ])
        );
    }
}

if (!function_exists('global_object')) {
    function global_object()
    {
        Action::run('inject_global_object');
    }
}

if (!function_exists('sidebar')) {
    function sidebar(...$args)
    {
        ob_start();

        dynamic_sidebar(...$args);

        return ob_get_clean();
    }
}

if (!function_exists('nav')) {
    function nav($location, $walker = 'Walker_Nav_Menu', $options = [])
    {
        $before = '';
        $sticky = collect();

        Filter::add('wp_nav_menu_objects', function ($sorted_menu_items, $args) use ($sticky) {
            return collect($sorted_menu_items)->each(function ($item) use ($sticky) {
                return tap($item, function ($item) use ($sticky) {
                    if (collect($item->classes)->contains('is-sticky')) {
                        $sticky->push($item);
                    }
                });
            })->toArray();
        }, 10, 2);

        $args = array_merge([
            'theme_location' => $location,
            'walker' => new $walker(),
            'container' => false,
            'container_id' => false,
            'menu_id' => $location,
            'menu_class' => 'nav navbar-nav',
            'echo' => false
        ], $options);

        $output = wp_nav_menu($args);

        if ($sticky->count()) {
            $before .= '<ul class="nav nav--sm flex flex--middle">';
            $before .= call_user_func_array([new $walker(), 'walk'], [$sticky->all(), 0, $args]);
            $before .= '</ul>';
        }

        return $before . $output;
    }
}

if (!function_exists('partial')) {
    function partial($partial, $__data = [], $return = false)
    {
        $partial = str_replace('.', '/', $partial);

        do_action('get_template_part_partial', 'partial', $partial);

        $templates = [];

        if ($partial && is_string($partial)) {
            $templates[] = "{$partial}.php";
        }

        $templates = array_map(function ($template) {
            return "theme/views/{$template}";
        }, $templates);

        extract($__data);

        if ($file = locate_template($templates)) {
            if ($return) {
                ob_start();
                include $file;

                return ob_get_clean();
            }

            include $file;
        }
    }
}

if (!function_exists('wp_get_or_set_cache')) {
    function wp_get_or_set_cache($key, $update = false)
    {
        if ($data = wp_cache_get($key)) {
            return $data;
        }

        $data = is_callable($update) ? $update() : $update;

        wp_cache_set($key, $data);

        return $data;
    }
}

if (!function_exists('is_login')) {
    function is_login()
    {
        return $GLOBALS['pagenow'] === 'wp-login.php';
    }
}

if (!function_exists('request')) {
    /**
     * Get an instance of the current request or an input item from the request.
     *
     * @param  array|string                          $key
     * @param  mixed                                 $default
     * @return \Illuminate\Http\Request|string|array
     */
    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('request');
        }

        if (is_array($key)) {
            return app('request')->only($key);
        }

        return app('request')->input($key, $default);
    }
}

if (!function_exists('redirect')) {
    function redirect($url)
    {
        header(sprintf('Location: %s', $url));
        exit;
    }
}

if (!function_exists('query')) {
    function query($key = null)
    {
        global $wp_query;
        static $query;

        if (!$query) {
            $query = $wp_query;
        }

        if ($key) {
            return $key === 'vars'
                ? (object) $query->query_vars
                : (isset($query->query[$key])
                    ? $query->query[$key]
                    : $query->$key);
        }

        return $query;
    }
}

if (!function_exists('e')) {
    /**
     * Escape HTML entities in a string.
     *
     * @param string $value
     *
     * @return string
     */
    function e($value)
    {
        return htmlentities($value, ENT_QUOTES, 'UTF-8', false);
    }
}

if (!function_exists('starts_with')) {
    /**
     * Determine if a given string starts with a given substring.
     *
     * @param string       $haystack
     * @param string|array $needles
     *
     * @return bool
     */
    function starts_with($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if ($needle != '' && strpos($haystack, $needle) === 0) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('array_is_sequential')) {
    /**
     * Check if an array is sequential (have keys from 0 to n) or not.
     *
     * @param array $array The array to check.
     *
     * @return bool
     */
    function array_is_sequential($array)
    {
        return array_keys($array) === range(0, count($array) - 1);
    }
}

if (!function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (!function_exists('with')) {
    /**
     * Return the given object. Useful for chaining.
     *
     * @param mixed $object
     *
     * @return mixed
     */
    function with($object)
    {
        return $object;
    }
}

if (!function_exists('app')) {
    /**
     * Helper function to quickly retrieve an instance.
     *
     * @param null  $abstract   The abstract instance name.
     * @param array $parameters
     *
     * @return mixed
     */
    function app($abstract = null, array $parameters = [])
    {
        if (is_null($abstract)) {
            return Bright\Foundation\Framework::instance()->container;
        }

        return Bright\Foundation\Framework::instance()->container->make($abstract, $parameters);
    }
}

if (!function_exists('bright')) {
    /**
     * Helper function to retrieve the Bright class instance.
     *
     * @return Bright
     */
    function bright()
    {
        if (!class_exists('Bright')) {
            wp_die('Bright has not yet been initialized. Please make sure the Bright framework is installed.');
        }

        return Bright::instance();
    }
}

if (!function_exists('view')) {
    /**
     * Helper function to build views.
     *
     * @param string $view      The view relative path, name.
     * @param array  $data      Passed data.
     * @param array  $mergeData
     *
     * @return \Illuminate\View\Factory
     */
    function view($view = null, array $data = [], array $mergeData = [])
    {
        $factory = app('view');

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($view, $data, $mergeData);
    }
}

if (!function_exists('join_path')) {
    function join_path(...$parts)
    {
        return Bright\Support\Path::join(...$parts);
    }
}

if (!function_exists('generate_salt')) {
    function generate_salt()
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~!@#$%^&*()-_=+[]{}|;:<>,./? ';
        $salt = '';

        for ($count = 0; $count < 64; $count++) {
            $character_index = rand(0, strlen($characters) - 1);
            $salt .= $characters[$character_index];
        }

        return $salt;
    }
}

if (!function_exists('read_composer')) {
    function read_composer()
    {
        $file = sprintf('%s/composer.json', ROOT_DIR);

        return json_decode(file_get_contents($file), true);
    }
}

if (!function_exists('write_composer')) {
    function write_composer($data)
    {
        $file = sprintf('%s/composer.json', ROOT_DIR);

        return file_put_contents($file, str_replace('\/', '/', json_encode($data, JSON_PRETTY_PRINT)));
    }
}

if (!function_exists('is_https')) {
    /**
     * determines if currect request is secure
     * @return boolean
     */
    function is_https()
    {
        return is_ssl();
    }
}

if (!function_exists('quick_options')) {
    function quick_options($page, $section, $fields)
    {
        add_settings_section($section, Str::title($section), function ($args) use ($page) {
            do_settings_sections($args['id']);
        }, $page);

        foreach ($fields as $field) {
            register_setting($page, $field);
            add_settings_field($field, '<label for="'. $field .'">'. Str::title($field) .'</label>', function () use ($section, $field) {
                get_template_part('inc/settings/'. $section, $field);
            }, $page, $section);
        }
    }
}

if (!function_exists('guid')) {
    /**
     * generate a GUID, [Globally unique identifier](https://en.wikipedia.org/wiki/Globally_unique_identifier)
     * @param  boolean $prefix optional prefix
     * @param  boolean $braces optionally wrap generated id in braces
     * @return string  GUID formatted random string
     */
    function guid($prefix = false, $braces = false)
    {
        mt_srand((float) microtime() * 10000);
        $charid = strtoupper(md5(uniqid($prefix === false ? rand() : $prefix, true)));
        $uuid = implode(chr(45), [
            substr($charid, 0, 8),
            substr($charid, 8, 4),
            substr($charid, 12, 4),
            substr($charid, 16, 4),
            substr($charid, 20, 12)
        ]);

        return $braces ? chr(123) . $uuid . chr(125) : $uuid;
    }
}

if (!function_exists('wp_get_attachment')) {
    function wp_get_attachment($attachment_id)
    {
        $attachment = get_post($attachment_id);

        if (!$attachment) {
            return false;
        }

        return (object) [
            'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
            'caption' => $attachment->post_excerpt,
            'description' => $attachment->post_content,
            'href' => get_permalink($attachment->ID),
            'src' => $attachment->guid,
            'title' => $attachment->post_title
        ];
    }
}

if (!function_exists('array_list')) {
    /**
     * create a human readable list of array items
     * @param  array  $array array of items to be listed
     * @param  string $key   optional key for value in array of objects
     * @return string
     */
    function array_list($array, $key = null)
    {
        $string = '';
        foreach ($array as $i => $item) {
            if ($i && count($array) > 2) {
                $string .= ', ';
            }

            if (count($array) > 1 && count($array) === ($i + 1)) {
                $string .= ' and ';
            }

            if (is_callable($key)) {
                $string .= $key($item);
            } else {
                $string .= isset($item->$key) ? $item->$key : $item;
            }
        }

        return $string;
    }
}

if (!function_exists('simple_format')) {
    /**
     * Kind of, barely, somewhat of a stripped down markdown parser. Just paragraphs with italics, bold, and links.
     * @param  string $body  Plain text to be parsed
     * @param  string $tag   Option to replace p tags with something else
     * @param  array  $attrs Adds whatever key/values provided as attributes to the paragraph tags
     * @return string Generated HTML.
     */
    function simple_format($body, $tag = 'p', $attrs = [])
    {
        $attrs = attr($attrs);
        $opening_tag = '<'. $tag . $attrs .'>';

        $body = $opening_tag. preg_replace('/\n\s?\n/', '</'. $tag .'>'. $opening_tag, $body) .'</'. $tag .'>';

        $body = preg_replace('/\n/', '<br>', $body);

        $body = preg_replace_callback('/``([^\`]+)``/', function ($matches) {
            return '<em>'. $matches[1] . '</em>';
        }, $body);

        $body = preg_replace_callback('/\*\*([^\*]+)\*\*/', function ($matches) {
            return '<strong>'. $matches[1] . '</strong>';
        }, $body);

        $body = preg_replace_callback('/\[([^\]]+)\]\(([^\)]+)\)/', function ($matches) {
            return '<a href="'. $matches[2] .'">'. $matches[1] .'</a>';
        }, $body);

        return $body;
    }
}

if (!function_exists('assets')) {
    function assets($entries = null, $markup = false, $manifest = 'assets/manifest.json')
    {
        static $all;

        $results = [];
        $found_css = [];
        $manifest_path = join_path(ROOT_DIR, $manifest);

        if (!is_file($manifest_path)) {
            return $markup ? '' : [];
        }

        $all = $all ?: json_decode(file_get_contents($manifest_path), true);

        if (!$entries) {
            return $all;
        }

        if (is_array($entries)) {
            $type = null;
        } else {
            $type = pathinfo($entries, PATHINFO_DIRNAME);
            $entries = [$entries];
        }

        foreach ($entries as $entry) {
            $basename = pathinfo($entry, PATHINFO_FILENAME);
            $ext = pathinfo($entry, PATHINFO_EXTENSION);

            foreach ($all as $key => $value) {
                if ($key === $entry || preg_match("/^$type\/.*~$basename.*\.$ext$/", $key)) {
                    switch ($type) {
                        case 'js' :
                            $result = $markup ? sprintf(
                                '<script src="%s" %s async defer></script>',
                                $value,
                                is_array($markup) ? attr($markup) : ''
                            ) : $value;
                            break;

                        case 'css' :
                            $result = $markup ? sprintf(
                                '<link href="%s" rel="stylesheet" %s>',
                                $value,
                                is_array($markup) ? attr($markup) : ''
                            ) : $value;
                            break;
                        default :
                            if (count($entries) === 1) {
                                return $value;
                            }

                            $result = $value;
                            break;
                    }

                    $id = sanitize_title(str_replace('~', '-', implode(' ', [
                        pathinfo($key, PATHINFO_FILENAME),
                        pathinfo($key, PATHINFO_EXTENSION)
                    ])));

                    $results[$id] = $result;
                }
            }
        }

        return $markup ? implode("\n", $results) . "\n" : $results;
    }
}

if (!function_exists('preload')) {
    function preload($as, $resource)
    {
        $pushed = [];
        $prev = (isset($_COOKIE['pushed']) && $data = json_decode($_COOKIE['pushed']))
            ? $data
            : [];

        if (!is_array($resource)) {
            $resource = [$resource];
        }

        foreach ($resource as $r) {
            $ext = pathinfo($r, PATHINFO_EXTENSION);
            $pushed[] = $r;
            if (!in_array($r, $prev)) {
                header(sprintf('Link: <%s>; as=%s; rel=preload;', $r, $as), false);
            }
        }

        setcookie('pushed', json_encode($pushed), 0, (60 * 60 * 24 * 30 + time()));

        return $resource;
    }
}

if (!function_exists('url')) {
    /**
     * Shorter funciton for site_url(). Also, takes care of custom WP directory beign prepended to path.
     *
     * @param  string $path   relative path
     * @param  string $scheme e.g. http, https, ftp, etc…
     * @return string fully qualified url
     */
    function url(...$path)
    {
        return home_url(join_path(...$path), is_https() ? 'https' : 'http');
    }
}

if (!function_exists('attr')) {
    /**
     * Helper function for getting an HTML attributes string from an associative array
     *
     * @param  array  $attributes associative array of attribute keys and values
     * @param  array  $except     array of blacklisted attributes
     * @return string string attributes for HTML elements
     */
    function attr($attributes = [], $except = [])
    {
        $html = [];

        foreach ((array) $attributes as $key => $value) {
            if (!is_null($value)) {
                if (is_numeric($key)) {
                    if (!in_array($value, (array) $except)) {
                        $pair = $value;
                    }
                } else {
                    if (!in_array($key, (array) $except)) {
                        $pair = $key .'="'. $value .'"';
                    }
                }
            }

            if (!is_null($pair)) {
                $html[] = $pair;
            }
        }

        return count($html) > 0 ? ' '.implode(' ', $html) : '';
    }
}

if (!function_exists('env')) {
    /**
     * Gets the value of an environment variable. Supports boolean, empty and null.
     *
     * @param  string $key
     * @param  mixed  $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {

            case 'true':
                return true;

            case 'false':
                return false;

            case 'empty':
                return '';

            case 'null':
                return;
        }

        return $value;
    }
}

if (!function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param  mixed $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (!function_exists('obfuscate')) {
    /**
     * simple way to help protected printed emails from bots
     *
     * @param  string $str email/string to be obfuscated
     * @return string encoded string
     */
    function obfuscate($str)
    {
        $str = mb_convert_encoding($str, 'UTF-32', 'UTF-8');
        $t = unpack('N*', $str);
        $t = array_map(function ($n) {
            return sprintf('&#%s;', $n);
        }, $t);

        return implode('', $t);
    }
}

if (!function_exists('safe_encode')) {
    /**
     * json encodes passed array/object and then encodes the string to base64 to guarenty there are no encoding issues when writing/reading from database
     *
     * @param  array/object $item array or object to be encoded
     * @return string       encoded string
     */
    function safe_encode($item)
    {
        return base64_encode(json_encode($item));
    }
}

if (!function_exists('safe_decode')) {
    /**
     * decodes strings generated by safe_encode()
     *
     * @param  string $item encoded string
     * @return object
     */
    function safe_decode($item)
    {
        return json_decode(base64_decode($item));
    }
}

if (!function_exists('prp')) {
    /**
     * Helper function for printing arrays/objects in <pre> tags
     *
     * @param  any    $item value to print
     * @return string
     */
    function prp(...$items)
    {
        $items = array_map(function ($item) {
            return print_r($item, true);
        }, $items);

        $string = '<pre>';
        $string .= implode("\n", $items);
        $string .= '</pre>';

        return $string;
    }
}

if (!function_exists('pvd')) {
    /**
     * Helper function for var dumping in pretags
     *
     * @param  any    $item value to var_dump()
     * @return string
     */
    function pvd(...$items)
    {
        ob_start();

        echo '<pre>';

        foreach ($items as $i => $item) {
            if ($i) {
                echo "\n";
            }

            var_dump($item);
        }

        echo '</pre>';

        return ob_get_clean();
    }
}

if (!function_exists('hm_add_rewrite_rule')) {
    /**
     * The main wrapper function for the WP\Http\HMRewrite API. Use this to add new rewrite rules
     *
     * Create a new rewrite with the arguments listed below. The only required argument is 'regex'
     *
     * @param string   $regex                The rewrite regex, start / end delimter not required. Eg: '^people/([^/]+)/?'
     * @param mixed    $query                The WP_Query args to be used on this "page"
     * @param string   $template             The template file used to render the request. Not required, will use
     *                                       the template file for the WP_Query if not set. Relative to template_directory() or absolute.
     * @param string   $body_class           A class to be added to body_class in the rendered template
     * @param function $body_class_callback  A callback that will be hooked into body_class
     * @param function $request_callback     A callback that will be called as soon as the request matching teh regex is hit.
     *                                       called with the WP object
     * @param function $parse_query_callback A callback taht will be hooked into 'parse_query'. Use this to modify query vars
     *                                       in the main WP_Query
     * @param function $title_callback       A callback that will be hooked into wp_title
     * @param function $query_callback       A callback taht will be called once the WP_Query has finished. Use to overrite any
     *                                       annoying is_404, is_home etc that you custom query may not match to.
     * @param function $access_rule          An access rule for restriciton to logged-in-users only for example.
     * @param array    $request_methods      An array of request methods, e.g. PUT, POST
     * @param mixed    $args
     */
    function hm_add_rewrite_rule($args = [])
    {

        // backwards compat
        if (count(func_get_args()) > 1 && is_string($args)) {
            $provided = func_get_args();

            $args = [];
            $args['regex'] = $provided[0];
            $args['query'] = $provided[1];

            if (count($provided) > 2) {
                $args['template'] = $provided[2];
            }

            if (count($provided) > 3) {
                $args = array_merge($args, $provided[3]);
            }
        }

        if (!empty($args['rewrite'])) {
            $args['regex'] = $args['rewrite'];
        }

        $regex = $args['regex'];

        $rule = new WP\Http\HMRewriteRule($regex, isset($args['id']) ? $args['id'] : null);

        if (!empty($args['template'])) {
            $rule->set_template($args['template']);
        }

        if (!empty($args['body_class_callback'])) {
            $rule->add_body_class_callback($args['body_class_callback']);
        }

        if (!empty($args['request_callback'])) {
            $rule->add_request_callback($args['request_callback']);
        }

        if (!empty($args['parse_query_callback'])) {
            $rule->add_parse_query_callback($args['parse_query_callback']);
        }

        if (!empty($args['title_callback'])) {
            $rule->add_title_callback($args['title_callback']);
        }

        if (!empty($args['query_callback'])) {
            $rule->add_query_callback($args['query_callback']);
        }

        if (!empty($args['access_rule'])) {
            $rule->set_access_rule($args['access_rule']);
        }

        if (!empty($args['query'])) {
            $rule->set_wp_query_args($args['query']);
        }

        if (!empty($args['permission'])) {
            $rule->set_access_rule($args['permission']);
        }

        if (!empty($args['admin_bar_callback'])) {
            $rule->add_admin_bar_callback($args['admin_bar_callback']);
        }

        if (!empty($args['disable_canonical'])) {
            $rule->disable_canonical = true;
        }

        // some convenenience properties. These are done here because they are not very nice
        if (!empty($args['body_class'])) {
            $rule->add_body_class_callback(function ($classes) use ($args) {
                $classes[] = $args['body_class'];

                return $classes;
            });
        }

        if (!empty($args['parse_query_properties'])) {
            $rule->add_parse_query_callback(function (WP_Query $query) use ($args) {
                $args['parse_query_properties'] = wp_parse_args($args['parse_query_properties']);
                foreach ($args['parse_query_properties'] as $property => $value) {
                    $query->$property = $value;
                }
            });
        }

        if (!empty($args['post_query_properties'])) {
            $rule->add_query_callback(function (WP_Query $query) use ($args) {
                $args['post_query_properties'] = wp_parse_args($args['post_query_properties']);

                foreach ($args['post_query_properties'] as $property => $value) {
                    $query->$property = $value;
                }
            });
        }

        if (!empty($args['request_methods'])) {
            $rule->set_request_methods($args['request_methods']);
        }

        if (!empty($args['request_method'])) {
            $rule->set_request_methods([$args['request_method']]);
        }

        WP\Http\HMRewrite::add_rule($rule);
    }
}

if (!function_exists('hm_rewrite_flush')) {
    /**
     * Flush rewrite rules, without deleting the option
     *
     * Uses `update_option` without first using `delete_option`, allowing it to run
     * on every request.
     */
    function hm_rewrite_flush()
    {
        do_action('hm_rewrite_before_flush');

        global $wp_rewrite;
        $wp_rewrite->matches = 'matches';
        $rules = $wp_rewrite->rewrite_rules(true);

        if (update_option('rewrite_rules', $rules)) {
            do_action('hm_rewrite_flushed', $rules);
        } else {
            do_action('hm_rewrite_cached', $rules);
        }
    }
}
