<?php

namespace Bright\Shortcode;

use Illuminate\Support\Str;

class Shortcode
{
    public $attributes = [];

    public $options = [];

    public $id;

    public function __construct()
    {
        $name = class_basename($this);
        $this->id = $this->id ?: Str::snake($name, '-');

        add_shortcode($this->id, function ($attr, $content) {
            $this->options = $attr;

            return sprintf(
                '<div class="shortcode shortcode-%s">%s</div>',
                $this->id,
                $this->render(shortcode_atts($this->attributes, $this->options), $content)
            );
        });
    }

    public static function boot()
    {
        $class = get_called_class();

        new $class();
    }
}
