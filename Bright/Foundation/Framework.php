<?php

namespace Bright\Foundation;

use Bright\Support\Path;
use Bright\Facades\Action;
use Bright\Facades\Facade;

class Framework
{
    /**
     * The service container.
     *
     * @var Bright\Foundation\Application
     */
    public $container;
    /**
     * Bright instance.
     *
     * @var Bright
     */
    protected static $instance = null;

    /**
     * Framework version.
     *
     * @var float
     */
    const VERSION = '1.0';

    private function __construct()
    {
        $this->autoload();
        $this->bootstrap();
    }

    /**
     * Retrieve Bright class instance.
     *
     * @return Bright
     */
    public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Enqueue Admin scripts.
     */
    public function adminEnqueueScripts()
    {
        /*
         * Make sure the media scripts are always enqueued.
         */
        wp_enqueue_media();
    }

    /**
     * Output a global JS object in the <head> tag for the admin.
     * Allow developers to add JS data for their project in the admin area only.
     */
    public function adminHead()
    {
        Action::run('inject_global_object');
    }

    /**
     * Check for the composer autoload file.
     */
    protected function autoload()
    {
        // Check if there is a autoload.php file.
        // Meaning we're in development mode or
        // the plugin has been installed on a "classic" WordPress configuration.
        if (file_exists($autoload = __DIR__.'/vendor/autoload.php')) {
            require $autoload;

            // Developers using the framework in a "classic" WordPress
            // installation can activate this by defining
            // a BRIGHT_ERROR constant and set its value to true or false
            // depending of their environment.
            if (defined('BRIGHT_ERROR') && BRIGHT_ERROR) {
                $whoops = new \Whoops\Run();
                $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
                $whoops->register();
            }
        }
    }

    /**
     * Bootstrap the core plugin.
     */
    protected function bootstrap()
    {
        /*
         * Instantiate the service container for the project.
         */
        $this->container = new Application();

        /*
         * Define core framework paths.
         * These are real paths, not URLs to the framework files.
         */
        $this->container->paths([
            'storage' => Path::join(ROOT_DIR, 'storage'),
            'theme' => Path::join(ROOT_DIR, 'theme')
        ]);

        if (!is_dir($storage = $this->container->make('path.storage'))) {
            mkdir($storage);
        }

        if (!is_dir($theme = $this->container->make('path.theme'))) {
            mkdir($theme);
        }

        /*
         * Setup the facade.
         */
        Facade::setFacadeApplication($this->container);

        /*
         * Register into the container, the registered paths.
         * Normally at this stage, plugins should have
         * their paths registered into the $GLOBALS array.
         */
        // $this->container->registerAllPaths();

        /*
         * Register core service providers.
         */
        $this->registerProviders();

        /*
         * Project hooks.
         * Added in their called order.
         */
        add_action('admin_enqueue_scripts', [$this, 'adminEnqueueScripts']);
        add_action('admin_head', [$this, 'adminHead']);
        // add_action('template_redirect', 'redirect_canonical');
        // add_action('template_redirect', 'wp_redirect_admin_locations');
    }

    /**
     * Register core framework service providers.
     */
    protected function registerProviders()
    {
        /*
         * Service providers.
         */
        $providers = apply_filters('bright_service_providers', [
            \Bright\Asset\AssetServiceProvider::class,
            \Bright\Config\ConfigServiceProvider::class,
            \Bright\Database\DatabaseServiceProvider::class,
            \Bright\Finder\FinderServiceProvider::class,
            \Bright\Hook\HookServiceProvider::class,
            \Bright\Http\HttpServiceProvider::class,
            \Bright\Load\LoaderServiceProvider::class,
            \Bright\Log\LogServiceProvider::class,
            \Bright\Wordpress\PostTypeServiceProvider::class,
            \Bright\Validation\ValidationServiceProvider::class,
            \Bright\View\ViewServiceProvider::class
        ]);

        foreach ($providers as $provider) {
            $this->container->register($provider);
        }
    }
}
