<?php

namespace Bright\Foundation;

use Illuminate\Container\Container;

class Application extends Container
{
    /**
     * Project paths.
     *
     * @var array
     */
    protected $paths = [];

    /**
     * The loaded service providers.
     *
     * @var array
     */
    protected $loadedProviders = [];

    public function __construct()
    {
        $this->registerApplication();
    }

    /**
     * Register the Application class into the container,
     * so we can access it from the container itself.
     */
    public function registerApplication()
    {
        // Normally, only one instance is shared into the container.
        static::setInstance($this);
        $this->instance('app', $this);
    }

    public function paths($name, $value = null)
    {
        if (is_string($name) && $value === null) {
            return $this->make(sprintf('path.%s', $name));
        }

        $paths = is_array($name) ? $name : [$name => $value];

        foreach ($paths as $key => $path) {
            $this->instance(sprintf('path.%s', $key), $path);
        }

        return true;
    }

    /**
     * Register a service provider with the application.
     *
     * @param \Bright\Foundation\ServiceProvider|string $provider
     * @param array                                     $options
     * @param bool                                      $force
     *
     * @return \Bright\Foundation\ServiceProvider
     */
    public function register($provider, array $options = [], $force = false)
    {
        if (!$provider instanceof ServiceProvider) {
            $provider = new $provider($this);
        }
        if (array_key_exists($providerName = get_class($provider), $this->loadedProviders)) {
            return;
        }
        $this->loadedProviders[$providerName] = true;
        $provider->register();

        if (method_exists($provider, 'boot')) {
            $provider->boot();
        }
    }
}
