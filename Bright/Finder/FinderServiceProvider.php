<?php

namespace Bright\Finder;

use Bright\Support\ServiceProvider;
use Illuminate\Filesystem\Filesystem;

class FinderServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('filesystem', function () {
            return new Filesystem();
        });
    }
}
