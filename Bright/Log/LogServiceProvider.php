<?php

namespace Bright\Log;

use Carbon\Carbon;
use Bright\Support\Path;
use Illuminate\Log\Writer;
use Monolog\Logger as Monolog;
use Bright\Support\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('logger', function () {
            $logger = new Writer(
                new Monolog(env('APP_ENV', 'production'))
            );

            if (!is_dir($logs = Path::join($this->app['path.storage'], 'logs'))) {
                mkdir($logs);
            }

            $filename = sprintf('%s.log', Carbon::now()->toDateString());
            $logger->useFiles(Path::join($logs, $filename), 'debug');

            return $logger;
        });
    }
}
