<?php

namespace Bright\Hook;

interface ActionObserver
{
    /**
     * Trigger method.
     */
    public function update();
}
