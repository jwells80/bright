<?php

namespace Bright\Hook;

class FilterBuilder extends Hook
{
    /**
     * Run all filters registered with the hook.
     *
     * @param string $hook The filter hook name.
     * @param mixed  $args
     *
     * @return mixed
     */
    public function run($hook, ...$args)
    {
        return apply_filters($hook, ...$args);
    }

    /**
     * Add a filter event for the specified hook.
     *
     * @param string          $name
     * @param \Closure|string $callback
     * @param int             $priority
     * @param int             $accepted_args
     */
    protected function addEventListener($name, $callback, $priority, $accepted_args)
    {
        $this->hooks[$name] = [$callback, $priority, $accepted_args];
        add_filter($name, $callback, $priority, $accepted_args);
    }
}
