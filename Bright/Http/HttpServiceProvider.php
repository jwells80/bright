<?php

namespace Bright\Http;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Bright\Support\ServiceProvider;

class HttpServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->instance('request', Request::capture());

        // $this->app->singleton('response', function ($container) {
        //     return new Response();
        // });
    }
}
