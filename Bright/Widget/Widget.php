<?php

namespace Bright\Widget;

use WP_Widget;
use Bright\Facades\View;
use Illuminate\Support\Str;

class Widget extends WP_Widget
{
    public $instantiated;

    public $replaces;

    public $selective_refresh;

    public $extract = [];

    public $view;

    public $parital;

    public function __construct()
    {
        $this->make();
    }

    public static function boot()
    {
        app('action')->add('widgets_init', function () {
            $class = get_called_class();

            if (isset($class::$replaces)) {
                unregister_widget($class::$replaces);
            }

            register_widget($class);
        });
    }

    public function make()
    {
        if ($this->replaces) {
            unregister_widget($this->replaces);
        }

        $this->instantiated = true;

        $name = trim(preg_replace_callback('/([A-Z])/', function ($matches) {
            return " {$matches[0]}";
        }, class_basename($this)));

        $id = Str::slug($name);

        $this->front = sprintf('%s-front', $id);
        $this->admin = sprintf('%s-admin', $id);

        parent::__construct($id, $name, ['customize_selective_refresh' => $this->selective_refresh]);
    }

    public function partial($partial, $data = [])
    {
        if (!$this->instantiated) {
            $this->make();
        }

        $this->extract = $data;
        $this->partial = $partial;
    }

    public function view($view, $data = [])
    {
        if (!$this->instantiated) {
            $this->make();
        }

        $this->extract = $data;
        $this->view = $view;
    }

    public function widget($args, $instance)
    {
        $this->context = $args;
        $this->data = $instance;

        if ($this->front()) {
            $this->render();
        }
    }

    public function form($instance)
    {
        $this->data = $instance;

        if ($this->admin()) {
            $this->render();
        }
    }

    public function render()
    {
        $data = array_merge($this->extract, [
            'widget' => $this
        ]);

        $this->data = array_merge([
            'title' => '',
            'before_title' => '',
            'after_title' => ''
        ], $this->data);

        if ($this->view) {
            echo view($this->view, $data)->render();
        } elseif ($this->partial) {
            echo partial($this->partial, $data, true);
        }
    }

    public function admin()
    {
        return true;
    }

    public function front()
    {
        return true;
    }

    public function getID(...$args)
    {
        return $this->get_field_id(...$args);
    }

    public function getName(...$args)
    {
        return $this->get_field_name(...$args);
    }
}
