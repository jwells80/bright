<?php

namespace Bright\Widget;

use Walker as WordpressWalker;

class Walker extends WordpressWalker
{
    public $tree_type = ['post_type', 'taxonomy', 'custom'];

    public $db_fields = ['parent' => 'menu_item_parent', 'id' => 'db_id'];

    public function start_lvl(&$output, $depth = 0, $args = [])
    {
        $output .= $this->startLevel($depth, (object) $args, $output) ?: '';
    }

    public function end_lvl(&$output, $depth = 0, $args = [])
    {
        $output .= $this->endLevel($depth, (object) $args, $output) ?: '';
    }

    public function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $output .= $this->startEl($item, $depth, (object) $args, $id, $output) ?: '';
    }

    public function end_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $output .= $this->endEl($item, $depth, (object) $args, $id, $output) ?: '';
    }

    public function startLevel($depth, $args, $output)
    {
        return '<ul>';
    }

    public function endLevel($depth, $args, $output)
    {
        return '</ul>';
    }

    public function startEl($item, $depth, $args, $id, $output)
    {
        $el = '<li>';

        $el .= '<a href="' . esc_url($item->url) . '">';
        $el .= apply_filters('the_title', $item->title, $item->ID);
        $el .= '</a>';

        return $el;
    }

    public function endEl($item, $depth, $args, $id, $output)
    {
        return '</li>';
    }
}
