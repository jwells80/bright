<?php

namespace Bright\Support;

class Path
{
    // static $posix;
    // static $win32;

    public static function getDelimiter()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return ';';
        }

        return ':';
    }

    public static function getSep()
    {
        return DIRECTORY_SEPARATOR;
    }

    public static function basename($path, $ext = '')
    {
        return basename($path, $ext);
    }

    public static function dirname($path, $levels = 0)
    {
        if ($levels) {
            return dirname($path, $levels);
        }

        return pathinfo($path)['dirname'];
    }

    public static function extname($path)
    {
        $parts = pathinfo($path);

        return str_replace($parts['filename'], '', $parts['basename']);
    }

    // static function format(Array $parts)
    // {

    // }

    public static function isAbsolute($path)
    {
        $regExp = '%^(?<wrappers>(?:[[:print:]]{2,}://)*)'
                . '(?<root>(?:[[:alpha:]]:/|/)?)'
                . '(?<path>(?:[[:print:]]*))$%';

        $parts = [];

        if (!preg_match($regExp, $path, $parts)) {
            return false;
        }

        if ($parts['root'] !== '') {
            return true;
        }

        return false;
    }

    public static function join(...$paths)
    {
        return preg_replace_callback('/([^:])\/+/', function ($matches) {
            return $matches[1] . '/';
        }, implode('/', (array) $paths));
    }

    public static function normalize($path)
    {
        return realpath($path);
    }

    public static function parse($path)
    {
    }

    public static function relative($from, $to)
    {
    }

    public static function resolve(...$paths)
    {
        $rev = array_reverse($paths);

        foreach ($rev as $current) {
            $path = self::join($current, $path ?? '');

            if (self::isAbsolute($path)) {
                return $path;
            }
        }

        return false;
    }
}
