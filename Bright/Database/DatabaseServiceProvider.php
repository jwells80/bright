<?php

namespace Bright\Database;

use Bright\Support\ServiceProvider;
use Illuminate\Database\Connectors\ConnectionFactory;

// use Illuminate\Database\Capsule\Manager as CapsuleManager;

class DatabaseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('resolver', function ($container) {
            return new ConnectionResolver($container, new ConnectionFactory($container));
        });

        $this->app->singleton('grammar', function ($container) {
            return new Grammar();
        });

        $this->app->singleton('db', function ($container) {
            global $wpdb;

            return new Database($wpdb, $container['grammar']);
        });
    }
}
