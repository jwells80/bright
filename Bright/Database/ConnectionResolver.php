<?php

namespace Bright\Database;

use Illuminate\Database\DatabaseManager;

class ConnectionResolver extends DatabaseManager
{
    public function connection($name = null)
    {
        return app('db');
    }

    public function getDefaultConnection()
    {
        return 'mysql';
    }

    protected function configuration($name)
    {
        global $table_prefix;

        return [
            'driver' => 'mysql',
            'host' => DB_HOST,
            'database' => DB_NAME,
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'charset' => DB_CHARSET,
            'collation' => DB_COLLATE,
            'prefix' => $table_prefix
        ];
    }
}
