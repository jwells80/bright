<?php

namespace Bright\Database\Relations;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class BelongsToOne extends BelongsToMany
{
    public function getResults()
    {
        return $this->first();
    }
}
