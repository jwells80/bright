<?php

namespace Bright\Asset;

use Bright\Support\ServiceProvider;

class AssetServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('asset.finder', function () {
            return new AssetFinder();
        });

        $this->app->bind('asset', function ($container) {
            return new AssetFactory($container['asset.finder'], $container);
        });
    }
}
