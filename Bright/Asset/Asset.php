<?php

namespace Bright\Asset;

use Bright\Hook\HookInterface;

class Asset implements AssetInterface
{
    /**
     * The default area where to load assets.
     *
     * @var string
     */
    protected $area = 'front';

    /**
     * Allowed areas.
     *
     * @var array
     */
    protected $allowedAreas = ['admin', 'login', 'customizer', 'editor'];

    /**
     * Type of the asset.
     *
     * @var string
     */
    protected $type;

    /**
     * WordPress properties of an asset.
     *
     * @var array
     */
    protected $args;

    /**
     * Asset key name.
     *
     * @var string
     */
    protected $key;

    /**
     * A list of all Asset instances.
     *
     * @var array
     */
    // protected static $instances;

    /**
     * A list of enqueued assets.
     *
     * @var array
     */
    protected $instantiated;

    /**
     * @var \Bright\Hook\ActionBuilder
     */
    protected $action;

    /**
     * @var \Bright\Html\HtmlBuilder
     */
    protected $html;

    /**
     * @var \Bright\Hook\FilterBuilder
     */
    protected $filter;

    protected $mce_plugins;

    /**
     * Build an Asset instance.
     *
     * @param string                     $type
     * @param array                      $args
     * @param \Bright\Hook\HookInterface $action
     * @param \Bright\Html\HtmlBuilder   $html
     * @param \Bright\Hook\HookInterface $filter
     */
    public function __construct($type, array $args, HookInterface $action, HookInterface $filter)
    {
        $this->type = $type;
        $this->args = $this->parse($args);
        $this->key = strtolower(trim($args['handle']));
        $this->action = $action;
        $this->filter = $filter;

        // $this->registerInstance();

        $hooks = [
            'wp_enqueue_scripts',
            'admin_enqueue_scripts',
            'login_init',
            'customize_preview_init'
        ];

        foreach ($hooks as $hook) {
            if ($action->done($hook) && !$this->instantiated) {
                $this->register();
            } else {
                $action->add($hook, [$this, 'install']);
            }
        }

        $filter->add('mce_external_plugins', [$this, 'install']);
    }

    /**
     * Register asset instances.
     * @param mixed $area
     */
    // protected function registerInstance()
    // {
    //     if (isset(static::$instances[$this->area][$this->key])) {
    //         return;
    //     }

    //     static::$instances[$this->area][$this->key] = $this;
    // }

    /**
     * Allow the developer to define where to load the asset.
     * Only 'admin', 'login' and 'customizer' are accepted. If none of those
     * values are used, simply keep the default front-end area.
     *
     * @param string $area Specify where to load the asset: 'admin', 'login' or 'customizer'.
     *
     * @return Asset
     */
    public function to($area)
    {
        if (is_string($area) && in_array($area, $this->allowedAreas)) {
            $this->area = $area;
        }

        return $this;
    }

    /**
     * Localize data for the linked asset.
     * Output JS object right before the script output.
     *
     * @param string $objectName The name of the JS variable that will hold the data.
     * @param mixed  $data       Any data to attach to the JS variable: string, boolean, object, array, ...
     *
     * @return Asset
     */
    public function localize($objectName, $data)
    {
        if ('script' === $this->type) {
            $this->args['localize'][$objectName] = $data;
        }

        return $this;
    }

    /**
     * Remove a declared asset.
     *
     * @return Asset
     * @param  mixed $data
     * @param  mixed $position
     */
    // public function remove()
    // {
    //     if ($this->isQueued()) {
    //         unset(static::$instances[$this->area][$this->key]);
    //     }

    //     return $this;
    // }

    /**
     * Tells if an asset is queued or not.
     *
     * @return bool
     */
    // public function isQueued()
    // {
    //     if (isset(static::$instances[$this->area][$this->key])) {
    //         return true;
    //     }

    //     return false;
    // }

    /**
     * Add inline code before or after the loaded asset.
     * Default to "after".
     *
     * @param string $data     The inline code to output.
     * @param string $position Accepts "after" or "before" as values. Note that position is only working for JS assets.
     *
     * @return Asset
     */
    public function inline($data, $position = 'after')
    {
        if ('script' === $this->type) {
            $args = [
                'data' => $data,
                'position' => $position,
            ];
        } elseif ('style' === $this->type) {
            $args = [
                'data' => $data,
            ];
        }

        $this->args['inline'][] = $args;

        return $this;
    }

    /**
     * Add attributes to the asset opening tag.
     *
     * @param array $atts The asset attributes to add.
     *
     * @return Asset
     */
    public function addAttributes(array $atts)
    {
        $key = $this->key;

        $replace = function ($tag, $atts, $append) use ($html) {
            if (false !== $pos = strrpos($tag, $append)) {
                $tag = substr_replace($tag, attr($atts), $pos).' '.trim($append);
            }

            return $tag;
        };

        if ('script' === $this->type) {
            $append = '></script>';
            $this->filter->add('script_loader_tag', function ($tag, $handle) use ($atts, $append, $replace, $key) {
                // Check we're only filtering the current asset and not all.
                if ($key === $handle) {
                    return $replace($tag, $atts, $append);
                }

                return $tag;
            });
        }

        if ('style' === $this->type) {
            $append = ' />';
            $this->filter->add('style_loader_tag', function ($tag, $handle) use ($atts, $append, $replace, $key) {
                // Check we're only filtering the current asset and not all.
                if ($key === $handle) {
                    return $replace($tag, $atts, $append);
                }

                return $tag;
            }, 4);
        }

        return $this;
    }

    /**
     * Manipulate the static::$instances variable
     * in order to separate each asset in its area.
     * @param null|mixed $mce_plugins
     */
    // protected function orderInstances()
    // {
    //     foreach (static::$instances as $area => $assets) {
    //         if ($this->area !== $area && array_key_exists($this->key, static::$instances[$area])) {
    //             unset(static::$instances[$area][$this->key]);
    //             static::$instances[$this->area][$this->key] = $this;
    //         }
    //     }
    // }

    /**
     * Install the appropriate asset depending of its area.
     */
    public function install($mce_plugins = null)
    {
        $from = current_filter();

        switch ($from) {
            // Front-end assets.
            case 'wp_enqueue_scripts':

                if ($this->area === 'front' && !$this->instantiated) {
                    $this->register();
                }

                break;

            // Wordpress Tinymce editor assets
            case 'mce_external_plugins':

                $this->mce_plugins = $mce_plugins;

                if ($this->area === 'editor' && !$this->instantiated) {
                    $this->register();
                }

                return $this->mce_plugins;

                break;

            // WordPress admin assets.
            case 'admin_enqueue_scripts':

                if ($this->area === 'admin' && !$this->instantiated) {
                    $this->register();
                }

                break;

            // Login assets.
            case 'login_init':

                if ($this->area === 'login' && !$this->instantiated) {
                    $this->register();
                }

                break;

            case 'customize_preview_init':

                if ($this->area === 'customizer' && !$this->instantiated) {
                    $this->register();
                }

                break;
        }
    }

    /**
     * Return the asset type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Return the asset properties. If $name isset, return its value.
     * If nothing is defined, return all properties.
     *
     * @param string $name The argument name.
     *
     * @return array|string
     */
    public function getArgs($name = '')
    {
        if (!empty($name) && array_key_exists($name, $this->args)) {
            return $this->args[$name];
        }

        return $this->args;
    }

    /**
     * Return the asset area.
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Return the asset key.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Parse defined asset properties.
     *
     * @param array $args The asset properties.
     *
     * @return mixed
     */
    protected function parse(array $args)
    {
        /*
         * Parse version.
         */
        $args['version'] = $this->parseVersion($args['version']);

        /*
         * Parse mixed.
         */
        $args['mixed'] = $this->parseMixed($args['mixed']);

        return $args;
    }

    /**
     * Parse the version number.
     *
     * @param string|bool|null $version
     *
     * @return mixed
     */
    protected function parseVersion($version)
    {
        if (is_string($version)) {
            if (empty($version)) {
                // Passing empty string is equivalent to set it to null.
                return;
            }
            // Return the defined string version.
            return $version;
        }
        if (is_null($version)) {
            // Return null.
            return;
        }

        // Version can only be a string or null. If anything else, return false.
        return false;
    }

    /**
     * Parse the mixed argument.
     *
     * @param $mixed
     *
     * @return string|bool
     */
    protected function parseMixed($mixed)
    {
        if ('style' === $this->type) {
            $mixed = (is_string($mixed) && !empty($mixed)) ? $mixed : 'all';
        } elseif ('script' === $this->type) {
            $mixed = is_bool($mixed) ? $mixed : false;
        }

        return $mixed;
    }

    /**
     * Register the asset.
     *
     * @param Asset $asset
     */
    protected function register()
    {
        // Avoid duplicate calls to each instance.
        // if ($this->getArea() !== $asset->getArea()) {
        //     return;
        // }

        if ($this->getArea() === 'editor') {
            $args = $this->getArgs();

            if ($this->getType() === 'script') {
                $this->mce_plugins[$args['handle']] = $args['path'];
            } else {
                add_editor_style($args['path']);
            }
        } else {
            // Register asset.
            if ($this->getType() === 'script') {
                $this->registerScript($this);
            } else {
                $this->registerStyle($this);
            }
        }

        // Add asset to list of called instances.
        $this->instantiated = true;
    }

    /**
     * Register a 'script' asset.
     *
     * @param Asset $asset
     */
    protected function registerScript()
    {
        $args = $this->getArgs();

        wp_enqueue_script($args['handle'], $args['path'], $args['deps'], $args['version'], $args['mixed']);

        // Add localized data for scripts.
        if (isset($args['localize']) && !empty($args['localize'])) {
            foreach ($args['localize'] as $objectName => $data) {
                wp_localize_script($args['handle'], $objectName, $data);
            }
        }

        // Pass the asset instance and register inline code.
        $this->registerInline($this);
    }

    /**
     * Register a 'style' asset.
     *
     * @param Asset $this
     */
    protected function registerStyle()
    {
        $args = $this->getArgs();

        if ($this->getArea() === 'login') {
            wp_deregister_style('login');
        }

        wp_enqueue_style($args['handle'], $args['path'], $args['deps'], $args['version'], $args['mixed']);

        // Pass the asset instance and register inline code.
        $this->registerInline($this);
    }

    /**
     * Register inline code.
     *
     * @param Asset $asset
     */
    protected function registerInline()
    {
        $args = $this->getArgs();

        if (empty($args) || !isset($args['inline'])) {
            return;
        }

        // Process if there are inline codes.
        $inlines = $args['inline'];

        foreach ($inlines as $inline) {
            if ('script' === $this->getType()) {
                wp_add_inline_script($args['handle'], $inline['data'], $inline['position']);
            } elseif ('style' === $this->getType()) {
                wp_add_inline_style($args['handle'], $inline['data']);
            }
        }
    }
}
