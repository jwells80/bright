<?php

namespace Bright\Wordpress;

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
    use Concerns\HasUserAttributes;

    public $meta = [
        'nickname',
        'first_name',
        'last_name',
        'description',
        'rich_editing',
        'comment_shortcuts',
        'admin_color',
        'use_ssl',
        'show_admin_bar_front',
        'locale',
        'creative_capabilities',
        'creative_user_level',
        'dismissed_wp_pointers',
        'show_welcome_panel',
        'session_tokens',
        'creative_dashboard_quick_press_last_post_id',
        'itsec_user_activity_last_seen',
        // 'creative_user-settings',
        // 'creative_user-settings-time',
        'nav_menu_recently_edited',
        // 'managenav-menuscolumnshidden',
        // 'metaboxhidden_nav-menus',
        'creative_media_library_mode'
    ];

    public $table = 'users';

    public $timestamps = false;

    protected $primaryKey = 'ID';

    protected $fillable = [
        'ID',
        'user_login',
        'user_pass',
        'user_nicename',
        'user_email',
        'user_url',
        'user_registered',
        'user_activation_key',
        'user_status',
        'display_name'
    ];

    public function postmeta()
    {
        return $this->hasMany('Bright\Wordpress\UserMeta', 'user_id');
    }

    public function __get($key)
    {
        if (in_array($key, $this->meta)) {
            return maybe_unserialize($this->postmeta->where('meta_key', $key)->pluck('meta_value')->first());
        }

        return $this->getAttribute($key);
    }

    public function __set($key, $value)
    {
        if (in_array($key, $this->meta)) {
            $meta = $this->postmeta->where('meta_key', $key)->first();

            return $meta ? $meta->meta_value = $value : $this->postmeta->push(PostMeta::create([
                'meta_key' => $key,
                'meta_value' => $value
            ]));
        }

        $this->setAttribute($key, $value);
    }
}
