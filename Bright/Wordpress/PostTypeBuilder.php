<?php

namespace Bright\Wordpress;

use Illuminate\Support\Str;
use Bright\Hook\HookInterface;

class PostTypeBuilder
{
    protected $types = [];

    public function __construct(HookInterface $action)
    {
        $action->add('init', [$this, 'registerAll']);
    }

    public function register($model)
    {
        $args = $this->build($model);

        $this->types[$model->type] = $args;
    }

    public function registerAll()
    {
        foreach ($this->types as $type => $args) {
            if (!post_type_exists($type)) {
                register_post_type($type, $args);

                if (isset($args['taxonomies'])) {
                    foreach ($args['taxonomies'] as $tax) {
                        app('taxonomy')->register(new $tax, $type);
                    }
                }
            }
        }
    }

    protected function build($model)
    {
        $arguments = collect($model->_defaults)->map(function ($value, $key) use ($model) {
            return isset($model->$key) ? $model->$key : $value;
        })->all();

        $args = [
            'label' => $arguments['label'],
            'labels' => $arguments['labels'] ?: $this->labels($model),
            'description' => $arguments['description'],
            'public' => $arguments['public'],
            'hierarchical' => $arguments['hierarchical'],
            'exclude_from_search' => $arguments['exclude_from_search'],
            'publicly_queryable' => $arguments['publicly_queryable'],
            'show_ui' => $arguments['show_ui'],
            'show_in_menu' => $arguments['show_in_menu'],
            'show_in_nav_menus' => $arguments['show_in_nav_menus'],
            'show_in_admin_bar' => $arguments['show_in_admin_bar'],
            'show_in_rest' => isset($arguments['show_in_rest'])
                ? $arguments['show_in_rest']
                : true,
            'rest_base' => $arguments['rest_base'],
            'rest_controller_class' => $arguments['rest_controller_class'],
            'menu_position' => (isset($arguments['menu_position']) && $arguments['menu_position'])
                ? $arguments['menu_position']
                : $arguments['position'],
            'menu_icon' => (isset($arguments['menu_icon']) && $arguments['menu_icon'])
                ? $arguments['menu_icon']
                : $arguments['icon'],
            'capability_type' => (isset($arguments['capability_type']) && $arguments['capability_type'])
                ? $arguments['capability_type']
                : $arguments['capability'],
            'capabilities' => $arguments['capabilities'],
            'map_meta_cap' => $arguments['map_meta_cap'],
            'supports' => isset($arguments['supports'])
                ? $arguments['supports']
                : ['title', 'editor', 'thumbnail', 'custom-fields'],
            'register_meta_box_cb' => $arguments['register_meta_box_cb'],
            'taxonomies' => $arguments['taxonomies'],
            'has_archive' => $arguments['has_archive'],
            'rewrite' => is_array($arguments['rewrite'])
                ? array_merge(['with_front' => false], $arguments['rewrite'])
                : $arguments['rewrite'],
            'query_var' => $arguments['query_var'],
            'can_export' => $arguments['can_export'],
            'delete_with_user' => $arguments['delete_with_user'],
        ];

        return collect($args)->filter(function ($value) {
            return $value !== null;
        })->all();
    }

    protected function labels($model)
    {
        $singular = Str::title(
            $model->singular
                ? str_replace(['-', '_'], ' ', $model->singular)
                : Str::singular(str_replace(['-', '_'], ' ', $model->type))
        );

        $plural = Str::title(
            $model->plural
                ? str_replace(['-', '_'], ' ', $model->plural)
                : Str::plural(str_replace(['-', '_'], ' ', $model->type))
        );

        return collect([
            'name' => $model->collective ?: $plural,
            'singular_name' => $singular,
            'add_new' => sprintf('Add %s', $singular),
            'add_new_item' => sprintf('New %s', $singular),
            'edit_item' => sprintf('Edit %s', $singular),
            'new_item' => sprintf('New %s', $singular),
            'view_item' => sprintf('View %s', $singular),
            'search_items' => sprintf('Search %s', $plural)
        ])->merge($model->labels ?? [])->all();
    }
}
