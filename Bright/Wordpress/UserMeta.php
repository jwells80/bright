<?php

namespace Bright\Wordpress;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Person.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class UserMeta extends Eloquent
{
    public $guarded = [];

    public $timestamps = false;

    public $table = 'usermeta';

    protected $primaryKey = 'umeta_id';

    public function user()
    {
        return $this->belongsTo('Bright\Wordpress\User');
    }
}
