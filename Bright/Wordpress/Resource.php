<?php

namespace Bright\Wordpress;

use Request;
use Bright\Facades\Filter;
use Illuminate\Support\Str;

class Resource
{
    use Concerns\HasPostTypeGroups;

    public $group = ['type' => 'any'];

    protected $post_type;

    protected $resources = [];

    public function __construct(PostTypeBuilder $post_type)
    {
        $this->post_type = $post_type;

        Filter::add('wp_insert_post_data', [$this, 'registerMethods'], 1, 2);
    }

    public function uses($handler)
    {
        $controller = $this->convertToControllerAction($handler);

        $this->resources[$this->group['type']] = $controller;
    }

    public function save($handler)
    {
        $this->resources[$this->group['type']]['save'] = $handler;
    }

    public function update($handler)
    {
        $this->resources[$this->group['type']]['update'] = $handler;
    }

    public function group($args, $group)
    {
        global $pagenow;

        if (is_string($args)) {
            $args = ['type' => $args];
        }

        $this->group = $args;

        if (request('post_type') === app($args['type'])->type
            || get_post_type(request('post')) === app($args['type'])->type
            || (($pagenow === 'edit.php') && app($args['type'])->type === 'post')) {
            $this->loadGroup($group);
        }

        $this->group = ['type' => 'any'];
    }

    public function addColumn($name, $markup_callback = null, $index = null)
    {
        $slug = false;

        if (is_array($name)) {
            $slug = Str::snake(array_keys($name)[0], '-');
            $name = array_shift($name);
        } else {
            $slug = Str::snake($name, '-');
            $name = Str::title(str_replace(['-', '_'], ' ', $name));
        }

        if (!is_callable($markup_callback) && $index === null) {
            $index = $markup_callback;
        }

        $index = is_int($index) ? $index : -1;

        $column = [
            $slug => $name
        ];

        add_filter(sprintf('manage_%s_posts_columns', app($this->group['type'])->type), function ($columns) use ($column, $index) {
            $end = array_splice($columns, $index);
            $columns = $columns + $column + $end;

            return $columns;
        });

        add_action(sprintf('manage_%s_posts_custom_column', app($this->group['type'])->type), function ($column_name, $post_id) use ($markup_callback, $slug) {
            if ($column_name === $slug) {
                if (is_callable($markup_callback)) {
                    echo $markup_callback($post_id);
                } else {
                    $item = get_post($post_id);
                    echo $item->$slug;
                }
            }
        }, 10, 2);
    }

    public function renameColumn($original, $new)
    {
        $original = Str::title(Str::singular(str_replace(['-', '_'], ' ', $original)));
        $new = Str::title(Str::singular(str_replace(['-', '_'], ' ', $new)));
        $slug = Str::snake($original, '-');

        add_filter(sprintf('manage_%s_posts_columns', app($this->group['type'])->type), function ($columns) use ($slug, $new) {
            if (isset($columns[$slug])) {
                $columns[$slug] = $new;
            }

            return $columns;
        });
    }

    public function registerMethods($data, $post)
    {
        if (isset($post['original_publish'])) {
            $data['ID'] = $post['ID'];

            switch ($post['original_publish']) {

                case 'Publish' :
                    $action = 'store';
                break;

                case 'Update' :
                    $action = 'update';
                break;

                default :
                    echo pvd($post['original_publish']);
                break;
            }

            $resources = collect($this->resources)->filter()->all();

            foreach ($resources as $model => $controller) {
                $type = Str::snake(class_basename($model), '-');

                if ($data['post_type'] === $type) {
                    if (class_exists($model)) {
                        $data = new $model($data);
                    }

                    if (isset($controller[$action]) && is_callable($controller[$action])
                        || is_callable($controller[$action = 'save'])) {
                        $controller[$action](request(), $data, $post);
                    } elseif (method_exists($controller['controller'], $action)
                        || method_exists($controller['controller'], $action = 'save')) {
                        app($controller['controller'])->$action(request(), $data, $post);
                    }

                    return Filter::run('insert_eloquent_model', $data);
                }
            }
        }

        return $data;
    }

    protected function loadGroup($group)
    {
        if (is_callable($group)) {
            $group();
        } else {
            require $group;
        }
    }
}
