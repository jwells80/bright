<?php

namespace Bright\Wordpress\Concerns;

trait HasPostTypeGroups
{
    protected function convertToControllerAction($action)
    {
        if (is_string($action)) {
            $action = ['uses' => $action];
        }

        $action['controller'] = $action['uses'];

        if (isset($this->post_type->group['namespace'])) {
            $action['controller'] = $this->prependGroupNamespace($action['controller']);
        }

        return $action;
    }

    protected function prependGroupNamespace($uses)
    {
        return implode('\\', [$this->post_type->group['namespace'], $uses]);
    }
}
