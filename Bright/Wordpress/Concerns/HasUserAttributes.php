<?php

namespace Bright\Wordpress\Concerns;

trait HasUserAttributes
{
    public function getIdAttribute()
    {
        return (int) isset($this->attributes['ID']) ? $this->attributes['ID'] : 0;
    }

    public function getPermalinkAttribute()
    {
        return get_author_posts_url($this->attributes['ID']);
    }

    public function getEmailAttribute()
    {
        return $this->attributes['user_email'];
    }

    public function setEmailAttribute($value)
    {
        return $this->attributes['user_email'] = $value;
    }

    public function getWebsiteAttribute()
    {
        return $this->attributes['user_url'];
    }

    public function setWebsiteAttribute($value)
    {
        return $this->attributes['user_url'] = $value;
    }

    public function getThumbnailAttribute()
    {
        return $this->thumbnail();
    }

    public function thumbnail($size = 64)
    {
        return get_avatar_url($this->attributes['user_email'], ['size' => $size]);
    }
}
