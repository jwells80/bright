<?php

namespace Bright\Wordpress\Concerns;

trait HasTaxonomyAttributes
{
    public $_defaults = [
        'label' => null,
        'labels' => null,
        'description' => null,
        'public' => true,
        'publicly_queryable' => null,
        'hierarchical' => null,
        'show_ui' => null,
        'show_in_menu' => null,
        'show_in_nav_menus' => null,
        'show_in_admin_bar' => null,
        'show_in_rest' => null,
        'rest_base' => null,
        'rest_controller_class' => null,
        'show_tagcloud' => null,
        'show_in_quick_edit' => null,
        'show_admin_column' => null,
        'meta_box_cb' => null,
        'capabilities' => null,
        'rewrite' => [],
        'query_var' => null,
        'update_count_callback' => null,
    ];

    public function getIdAttribute()
    {
        return (int) isset($this->attributes['ID']) ? $this->attributes['ID'] : 0;
    }
}
