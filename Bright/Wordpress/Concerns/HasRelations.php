<?php

namespace Bright\Wordpress\Concerns;

use Bright\Database\Relations\BelongsToOne;

trait HasRelations
{
    public function hasTaxonomy($taxonomy)
    {
        return $this->belongsToMany($taxonomy, 'term_relationships', 'object_id', 'term_taxonomy_id');
    }

    public function hasOneThroughMeta($model, $meta_key = null)
    {
        $meta_key = $meta_key ?: strtolower(
            $this->getForeignKey()
        );

        return $this->belongsToOne($model, 'postmeta', 'meta_value', 'post_id')
            ->where('meta_key', $meta_key);
    }

    public function hasManyThroughMeta($model, $meta_key = null)
    {
        $meta_key = $meta_key ?: strtolower(
            $this->getForeignKey()
        );

        return $this->belongsToMany($model, 'postmeta', 'meta_value', 'post_id')
            ->where('meta_key', $meta_key);
    }

    public function belongsToThroughMeta($model, $meta_key = null)
    {
        $meta_key = $meta_key ?: strtolower(
            $this->newRelatedInstance($model)
                 ->getForeignKey()
        );

        return $this->belongsToOne($model, 'postmeta', 'post_id', 'meta_value')
            ->where('meta_key', $meta_key);
    }

    public function belongsToManyThroughMeta($model, $meta_key = null)
    {
        $meta_key = $meta_key ?: strtolower(
            $this->newRelatedInstance($model)
                 ->getForeignKey()
        );

        return $this->belongsToMany($model, 'postmeta', 'post_id', 'meta_value')
            ->where('meta_key', $meta_key);
    }

    public function belongsToOne(
        $related,
        $table = null,
        $foreignPivotKey = null,
        $relatedPivotKey = null,
        $parentKey = null,
        $relatedKey = null,
        $relation = null
    ) {
        if (is_null($relation)) {
            $relation = $this->guessBelongsToManyRelation();
        }

        $instance = $this->newRelatedInstance($related);
        $foreignPivotKey = $foreignPivotKey ?: $this->getForeignKey();
        $relatedPivotKey = $relatedPivotKey ?: $instance->getForeignKey();

        if (is_null($table)) {
            $table = $this->joiningTable($related);
        }

        return new BelongsToOne(
            $instance->newQuery(),
            $this,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey ?: $this->getKeyName(),
            $relatedKey ?: $instance->getKeyName(),
            $relation
        );
    }
}
