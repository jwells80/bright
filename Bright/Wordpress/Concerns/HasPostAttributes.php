<?php

namespace Bright\Wordpress\Concerns;

use Config;
use Carbon\Carbon;
use Illuminate\Support\Str;

trait HasPostAttributes
{
    public $_defaults = [
        'label' => null,
        'labels' => null,
        'description' => null,
        'public' => true,
        'hierarchical' => null,
        'exclude_from_search' => null,
        'publicly_queryable' => null,
        'show_ui' => null,
        'show_in_menu' => null,
        'show_in_nav_menus' => null,
        'show_in_admin_bar' => null,
        'show_in_rest' => null,
        'rest_base' => null,
        'rest_controller_class' => null,
        'position' => null,
        'icon' => null,
        'capability' => null,
        'capabilities' => null,
        'map_meta_cap' => null,
        'supports' => null,
        'register_meta_box_cb' => null,
        'taxonomies' => null,
        'has_archive' => null,
        'rewrite' => [],
        'query_var' => null,
        'can_export' => null,
        'delete_with_user' => null,
    ];

    public function getIdAttribute()
    {
        return (int) isset($this->attributes['ID']) ? $this->attributes['ID'] : 0;
    }

    public function getPermalinkAttribute()
    {
        return get_permalink($this->attributes['ID']);
    }

    public function getEditPermalinkAttribute()
    {
        return get_edit_post_link($this->attributes['ID']);
    }

    public function getSlugAttribute()
    {
        return isset($this->attributes['post_name']) ? $this->attributes['post_name'] : null;
    }

    public function setSlugAttribute($value)
    {
        return $this->attributes['post_name'] = $value;
    }

    public function getContentAttribute()
    {
        global $post;

        $global = $post;
        $post = $this->attributes;
        $content = apply_filters('the_content', $this->attributes['post_content']);
        $post = $global;

        return $content;
    }

    public function setContentAttribute($value)
    {
        return $this->attributes['post_content'] = $value;
    }

    public function getCleanContentAttribute()
    {
        return trim(wp_strip_all_tags($this->attributes['post_content']));
    }

    public function getExcerptAttribute()
    {
        return $this->excerpt();
    }

    public function excerpt($max = 100, $end = '…', $do_shortcodes = true)
    {
        $content = $do_shortcodes
            ? $this->content
            : strip_shortcodes($this->clean_content);

        list($content) = $this->attributes['post_excerpt']
            ? $this->attributes['post_excerpt']
            : explode('<!--more-->', $content);

        $text = wp_strip_all_tags($content);
        $excerpt = wp_trim_words($text, $max, $end);

        return apply_filters('get_the_excerpt', $excerpt);
    }

    public function setExcerptAttribute($value)
    {
        return $this->attributes['post_excerpt'] = $value;
    }

    public function getStatusAttribute()
    {
        return $this->attributes['post_status'];
    }

    public function setStatusAttribute($value)
    {
        return $this->attributes['post_status'] = $value;
    }

    public function getPublishedAtAttribute()
    {
        return Carbon::parse($this->attributes['post_date']);
    }

    public function getUpdateAtAttribute()
    {
        return Carbon::parse($this->attributes['post_modified']);
    }

    public function getThumbnailAttribute()
    {
        return $this->thumbnail();
    }

    public function thumbnail($size = 'full', $query = [], $strict = true)
    {
        $sizes = Config::get('images')->all();

        if (is_bool($query)) {
            $strict = $query;
            $query = [];
        }

        $thumbnail = get_the_post_thumbnail_url($this->attributes['ID'], $size);

        if (!env('IMGIX_SOURCE') && $strict && isset($sizes[$size])) {
            list($width, $height) = $sizes[$size];

            if ($width && $height) {
                if (!is_int(strpos($thumbnail, implode('x', [$width, $height])))) {
                    return false;
                }
            }

            if ($width) {
                if (!is_int(strpos($thumbnail, "-{$width}x"))) {
                    return false;
                }
            }

            if ($height) {
                if (!is_int(strpos($thumbnail, "x{$height}."))) {
                    return false;
                }
            }
        }

        if (env('IMGIX_SOURCE') && $thumbnail) {
            $parts = explode('?', $thumbnail);
            parse_str($parts[1], $original_query);

            if (isset($query['x'])) {
                if (isset($original_query['w'])) {
                    $original_query['w'] *= (float) $query['x'];
                }

                if (isset($original_query['h'])) {
                    $original_query['h'] *= (float) $query['x'];
                }

                unset($query['x']);
            }

            return implode('?', [$parts[0], http_build_query(array_filter(array_merge($original_query, $query)))]);
        }

        return $thumbnail;
    }
}
