<?php

namespace Bright\Wordpress;

use Bright\Facades\Filter;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model as Eloquent;

class PostType extends Eloquent
{
    use Concerns\HasPostAttributes, Concerns\HasRelations;

    public $type;

    public $singular;

    public $plural;

    public $collective;

    public $meta = [];

    public $table = 'posts';

    protected $primaryKey = 'ID';

    protected $fillable = [
        'ID',
        'post_author',
        'post_date',
        'post_date_gmt',
        'post_content',
        'post_title',
        'post_excerpt',
        'post_status',
        'comment_status',
        'ping_status',
        'post_password',
        'post_name',
        'to_ping',
        'pinged',
        'post_modified',
        'post_modified_gmt',
        'post_content_filtered',
        'post_parent',
        'guid',
        'menu_order',
        'post_type',
        'post_mime_type',
        'comment_count',
        'filter'
    ];

    const CREATED_AT = 'post_date';

    const UPDATED_AT = 'post_modified';

    public function __construct($attributes = [])
    {
        $this->type = $this->type ?: Str::snake(class_basename($this), '-');

        if ($this->type === 'post-type') {
            $this->type = null;
        }

        app('post-type')->register($this);

        static::$resolver = app('resolver');

        parent::__construct((array) $attributes);

        if ($this->type) {
            $this->attributes['post_type'] = $this->type;
        }

        if ($this->ID) {
            $this->load('postmeta');
        }

        if (method_exists($this, 'register')) {
            $this->register();
        }
    }

    public static function boot()
    {
        new static;
    }

    public function newQuery()
    {
        $query = parent::newQuery()
            ->with('postmeta');

        if (!is_admin()) {
            $query->where('post_status', 'publish');
        }

        if ($this->type) {
            $query->where('post_type', $this->type);
        }

        return $query;
    }

    public static function collect($posts)
    {
        $model = get_called_class();
        $posts = ($posts instanceof \WP_Query) ? $posts->posts : $posts;

        return collect($posts)->map(function ($post) use ($model) {
            return new $model($post);
        })->all();
    }

    public function getNextAttribute()
    {
        $model = get_called_class();
        $post = get_adjacent_post(false, '', false);

        if (!$post) {
            return null;
        }

        return new $model($post);
    }

    public function getPrevAttribute()
    {
        $model = get_called_class();
        $post = get_adjacent_post(false, '', true);

        if (!$post) {
            return null;
        }

        return new $model($post);
    }

    public function scopeWhereMeta($query, $key, $value = true)
    {
        return $query->whereHas('postmeta', function ($query) use ($key, $value) {
            $query->where('meta_value', $value);
            $query->where('meta_key', $key);
        });
    }

    public function scopeOrderByMeta($query, $column, $direction = 'ASC')
    {
        return $query->select('posts.*')->join('postmeta as orderby', function ($join) use ($column) {
            $join->on('posts.ID', '=', 'orderby.post_id')
                ->where('orderby.meta_key', '=', $column);
        })->orderBy('orderby.meta_value', $direction);
    }

    public function postmeta()
    {
        return $this->hasMany(PostMeta::class, 'post_id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'post_author', 'ID');
    }

    public function isPublished()
    {
        return $this->status === 'publish';
    }

    public function commit(array $options = [])
    {
        return parent::save($options);
    }

    public function save(array $options = [])
    {
        Filter::add('insert_eloquent_model', function ($data) {
            $data = collect($data);

            if ($this->id === $data->get('ID')) {
                $data = $data->filter(function ($value, $key) {
                    return in_array($key, $this->fillable);
                });
            }

            return $data->forget('postmeta')->toArray();
        });

        $this->postmeta->map(function ($meta) {
            $meta->meta_value = preg_replace('/\\\\+([\'"]+)/', '$1', $meta->meta_value);

            return $meta;
        });

        $this->postmeta()->saveMany($this->postmeta);

        return true;
    }

    private function getMeta($key, $raw = false)
    {
        $value = $this->postmeta->where('meta_key', $key)->pluck('meta_value')->first();

        if (isset($this->casts[$raw ? $key.'_raw' : $key])) {
            $value = $this->castAttribute($raw ? $key.'_raw' : $key, $value);
        }

        if (!$raw && $this->hasGetMutator($key)) {
            $value = $this->mutateAttribute($key, $value);
        }

        return $value;
    }

    private function setMeta($key, $value)
    {
        $meta = $this->postmeta->where('meta_key', $key)->first();

        if ($this->hasSetMutator($key)) {
            $method = 'set'.Str::studly($key).'Attribute';
            $value = $this->{$method}($value);
        }

        $postmeta = PostMeta::firstOrNew([
            'post_id' => $this->id,
            'meta_key' => $key
        ]);

        $postmeta->meta_value = $value;

        return $meta ? $meta->meta_value = $value : $this->postmeta->push($postmeta);
    }

    public function __get($key)
    {
        $key = explode('_raw', $key);

        $raw = isset($key[1]);
        $key = $key[0];

        if (in_array($key, $this->meta)) {
            return $this->getMeta($key, $raw);
        }

        return $this->getAttribute($key);
    }

    public function __set($key, $value)
    {
        if (in_array($key, $this->meta)) {
            return $this->setMeta($key, $value);
        }

        $this->setAttribute($key, $value);
    }
}
