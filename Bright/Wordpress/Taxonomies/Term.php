<?php

namespace Bright\Wordpress\Taxonomies;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Bright\Wordpress\Concerns\HasTaxonomyAttributes;

/**
 *
 */
class Term extends Eloquent
{
    use HasTaxonomyAttributes;

    public $type;

    public $guarded = [];

    public $timestamps = false;

    public $table = 'terms';

    public $with = ['tax'];

    protected $primaryKey = 'term_id';

    public function __construct($attributes = [])
    {
        $this->type = $this->type ?: Str::snake(class_basename($this), '-');

        parent::__construct((array) $attributes);

        if (method_exists($this, 'register')) {
            $this->register();
        }
    }

    public static function boot()
    {
        new static;
    }

    public function newQuery()
    {
        return parent::newQuery()->whereHas('tax', function ($query) {
            $query->where('taxonomy', $this->type);
        });
    }

    public function belongsToType($model)
    {
        return $this->belongsToMany($model, 'term_relationships', 'term_taxonomy_id', 'object_id');
    }

    public function tax()
    {
        return $this->belongsTo(TermTaxonomy::class, 'term_id', 'term_id');
    }

    public function getPermalinkAttribute()
    {
        if (isset($this->attributes['term_id'], $this->tax->taxonomy)) {
            return get_term_link((int) $this->attributes['term_id'], $this->tax->taxonomy);
        }
    }

    public function link($key = 'name', $attr = [])
    {
        return sprintf('<a%s href="%s">%s</a>', attr($attr, ['href']), $this->permalink, $this->$key);
    }

    public function text($key = 'name', $attr = [])
    {
        return sprintf('<span%s>%s</span>', attr($attr), $this->$key);
    }
}
