<?php

namespace Bright\Wordpress\Taxonomies;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class Person.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class TermTaxonomy extends Pivot
{
    public $type;

    public $guarded = [];

    public $timestamps = false;

    public $table = 'term_taxonomy';

    protected $primaryKey = 'term_taxonomy_id';

    public function __construct($attributes = [])
    {
        $this->type = $this->type ?: Str::snake(class_basename($this), '-');

        parent::__construct((array) $attributes);
    }
}
