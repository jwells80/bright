<?php

namespace Bright\Wordpress\Taxonomies;

use Illuminate\Support\Str;

class TaxonomyBuilder
{
    protected $taxonomy;

    protected $types = [];

    public function register($taxonomy, $type = null)
    {
        $this->taxonomy = $taxonomy;

        $args = $this->build($taxonomy);

        if (!taxonomy_exists($taxonomy->type)) {
            register_taxonomy($taxonomy->type, $type, $args);
        }

        $this->types[$taxonomy->type] = $args;
    }

    public function build($tax)
    {
        $arguments = collect($tax->_defaults)->map(function ($value, $key) use ($tax) {
            return $tax->$key ?: $value;
        })->all();

        $args = [
            'label' => $arguments['label'],
            'labels' => $arguments['labels'] ?: $this->labels($tax),
            'description' => $arguments['description'],
            'public' => $arguments['public'],
            'publicly_queryable' => $arguments['publicly_queryable'],
            'hierarchical' => $arguments['hierarchical'],
            'show_ui' => $arguments['show_ui'],
            'show_in_menu' => $arguments['show_in_menu'],
            'show_in_nav_menus' => $arguments['show_in_nav_menus'],
            'show_in_admin_bar' => $arguments['show_in_admin_bar'],
            'show_in_rest' => isset($arguments['show_in_rest'])
                ? $arguments['show_in_rest']
                : true,
            'rest_base' => $arguments['rest_base'],
            'rest_controller_class' => $arguments['rest_controller_class'],
            'show_tagcloud' => $arguments['show_tagcloud'],
            'show_in_quick_edit' => $arguments['show_in_quick_edit'],
            'show_admin_column' => $arguments['show_admin_column'],
            'meta_box_cb' => $arguments['meta_box_cb'],
            'capabilities' => $arguments['capabilities'],
            'rewrite' => array_merge(['with_front' => false], $arguments['rewrite']),
            'query_var' => $arguments['query_var'],
            'update_count_callback' => $arguments['update_count_callback']
        ];

        return collect($args)->filter(function ($value) {
            return $value !== null;
        })->toArray();
    }

    protected function labels($tax)
    {
        $singular = Str::title(Str::singular(str_replace(['-', '_'], ' ', $tax->type)));
        $plural = Str::title(Str::plural(str_replace(['-', '_'], ' ', $tax->type)));

        return collect([
            'name' => $plural,
            'singular_name' => $singular,
            'search_items' => sprintf('Search %s', $plural),
            'popular_items' => sprintf('Popular %s', $plural),
            'all_items' => sprintf('All %s', $plural),
            'parent_item' => $tax->hierarchical
                ? sprintf('Parent %s', $singular)
                : null,
            'parent_item_colon' => $tax->hierarchical
                ? sprintf('Parent %s:', $singular)
                : null,
            'edit_item' => sprintf('Edit %s', $singular),
            'view_item' => sprintf('View %s', $singular),
            'update_item' => sprintf('Update %s', $singular),
            'add_new_item' => sprintf('Add New %s', $singular),
            'new_item_name' => sprintf('New %s Name', $singular),
            'separate_items_with_commas' => sprintf('Separate %s with commas', Str::lower($plural)),
            'add_or_remove_items' => sprintf('Add or remove %s', Str::lower($plural)),
            'choose_from_most_used' => sprintf('Choose from the most used %s', Str::lower($plural)),
            'not_found' => sprintf('No %s found.', Str::lower($plural)),
            'no_terms' => sprintf('No %s', Str::lower($plural)),
            'items_list_navigation' => sprintf('%s list navigation', $plural),
            'items_list' => sprintf('%s list', $plural),
        ])->filter(function ($label) {
            return $label !== null;
        })->merge($tax->labels ?? []);
    }
}
