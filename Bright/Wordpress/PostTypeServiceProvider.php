<?php

namespace Bright\Wordpress;

use Bright\Support\ServiceProvider;

class PostTypeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('post-type', function ($container) {
            return new PostTypeBuilder($container['action']);
        });

        $this->app->singleton('taxonomy', function ($container) {
            return new Taxonomies\TaxonomyBuilder($container['action']);
        });

        $this->app->singleton('metabox', function ($container) {
            return new MetaBox($container['resource']);
        });

        $this->app->singleton('resource', function ($container) {
            return new Resource($container['post-type']);
        });
    }
}
