<?php

namespace Bright\Wordpress;

use Illuminate\Support\Str;
use Illuminate\Routing\ControllerDispatcher;

class MetaBox
{
    use Concerns\HasPostTypeGroups;

    protected $resource;

    protected $screens = [];

    public function __construct(Resource $resource)
    {
        $this->resource = $resource;
        add_action('do_meta_boxes', [$this, 'render']);
    }

    public function add($name, $controller, $args = [])
    {
        if (empty($args) && is_array($controller)) {
            $args = $controller;
        }

        $this->register($name, array_merge(['uses' => $controller], array_merge([
            'if' => true,
            'context' => 'advanced',
            'priority' => 'default',
            'legacy' => false
        ], $args)));
    }

    public function register($name, $box)
    {
        $box = $this->convertToControllerAction($box);

        $this->screens[$this->resource->group['type']][$name] = $box;
    }

    public function render()
    {
        $this->screens = collect($this->screens)->filter();

        $this->screens->each(function ($boxes, $model) {
            $type = Str::snake(class_basename($model), '-');

            foreach ($boxes as $slug => $box) {
                $show = is_callable($if = $box['if']) ? $if($model) : $if;

                if ($show) {
                    add_meta_box(
                        Str::slug($slug),
                        str_replace('-', ' ', Str::title($slug)),
                        $this->createCallback($box, $model),
                        $type === 'any' ? null : $type,
                        $box['context'],
                        $box['priority'],
                        $box['legacy'] ? ['__back_compat_meta_box' => true] : null
                    );
                }
            }
        });
    }

    protected function createCallback($box, $model)
    {
        return function () use ($box, $model) {
            global $post;
            $eloquent_post;

            $post_id = $post->ID;

            if (is_string($box['controller'])) {
                list($class, $method) = explode('@', $box['controller']);
            } elseif (is_array($box['controller'])) {
                list($class, $method) = $box['controller'];
            }

            if (class_exists($model)) {
                $eloquent_post = $model::find($post_id) ?: new $model();
            }

            if (is_callable($box['controller'])) {
                echo $box['controller']($eloquent_post ?: $post);
            } else {
                echo call_user_func_array([app($class), $method], [$eloquent_post ?: $post]);
            }
        };
    }
}
