<?php

namespace Bright\Wordpress;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Person.
 * Help you retrieve data from your $prefix_posts table.
 *
 * @package Theme\Models
 */
class PostMeta extends Eloquent
{
    public $guarded = [];

    public $timestamps = false;

    public $table = 'postmeta';

    protected $primaryKey = 'meta_id';

    public function newQuery()
    {
        return parent::newQuery()->where('post_id', '!=', 0);
    }

    public function post()
    {
        return $this->belongsTo(PostType::class);
    }
}
