<?php

namespace Bright\Config;

class Menu
{
    /**
     * Save the menus list.
     *
     * @var array
     */
    protected $data = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Register the menus.
     *
     * @return Bright\Config\Menu
     */
    public function make()
    {
        if (is_array($this->data) && !empty($this->data)) {
            register_nav_menus($this->data);
        }

        return $this;
    }
}
