<?php

namespace Bright\Config;

class Loading
{
    /**
     * Save the retrieved datas.
     *
     * @var array
     */
    protected $data = [];

    protected $loader;

    public function __construct(array $data)
    {
        $this->loader = app('loader');
        $this->data = $data;
    }

    /**
     * @return Bright\Config\Loading
     */
    public function make()
    {
        $loader = app('loader');
        $this->loader->add($this->data)->load()->boot();

        return $this;
    }
}
