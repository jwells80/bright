<?php

namespace Bright\Config;

class Support
{
    /**
     * List of theme supports.
     */
    protected $data = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Add theme supports.
     * Call this only into your theme functions.php file or files relative to it.
     * If call outside of the theme, wrap this with `after_setup_theme` hook.
     *
     * @return Bright\Config\Support;
     */
    public function make()
    {
        if (is_array($this->data) && !empty($this->data)) {
            foreach ($this->data as $feature => $value) {
                if (is_int($feature)) {
                    // Theme features without options.
                    if ('post-formats' !== $value) {
                        $this->support($value);
                    }
                } else {
                    // Theme features with options.
                    $this->support($feature, $value);
                }
            }
        }

        return $this;
    }

    /**
     * Add theme support.
     *
     * @param string $feature
     * @param array  $value
     */
    protected function support(...$args)
    {
        add_theme_support(...$args);
    }
}
