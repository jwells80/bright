<?php

namespace Bright\Config;

class ConfigFactory implements ConfigInterface
{
    /**
     * Config file finder instance.
     *
     * @var ConfigFinder
     */
    protected $finder;

    public function __construct(ConfigFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * Return all or specific property from a config file.
     *
     * @param string $name The config file name or its property full name.
     *
     * @return mixed
     */
    public function get($name)
    {
        if (strpos($name, '.') !== false) {
            list($name, $property) = explode('.', $name);
        }

        $name = preg_replace_callback('/^\/*/', function () {
            return '/';
        }, $name);

        $path = $this->finder->find($name);
        $properties = include $path;

        // Looking for single property
        if (isset($property, $properties[$property])) {
            return collect($properties[$property]);
        }

        return collect($properties);
    }
}
