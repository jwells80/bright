<?php

namespace Bright\Load;

use Bright\Support\ServiceProvider;

class LoaderServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('loader', function () {
            return new Loader();
        });
    }
}
